import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

/// A Calculator.
class LoginService {
  var dio = new Dio();
  String _url;
  static String token;
  bool isLoading;
  Map userData = new Map();
  final storage = new FlutterSecureStorage();
  get getUrl => _url;
  get getData => userData;
  get getToken => token;
  get isOnProcess => isLoading;
  get isActive => !(token == null || token == "");
  setUrl(value) {
    _url = value;
  }

  isProcessing(value) {
    isLoading = value;
  }

  setData(value) {
    userData = value;
  }

  loginRequest() async {
    try {
      Response response = await dio.post(getUrl, data: getData);
      return {"hasError": false, "hasInternet": true, "data": response.data};
    } on DioError catch (e) {
      if (e.response == null) {
        return {
          "hasError": true,
          "hasInternet": false,
          "message": "No Internet Connection"
        };
      } else
        return {
          "hasError": true,
          "hasInternet": true,
          "message": e.response.statusMessage
        };
    } catch (e) {
      return {"hasError": true, "hasInternet": true, "message": 'ERROR'};
    }
  }

  showCircularProgressIndicator(context) {
    return showDialog(
      child: AlertDialog(
        title: Center(
            child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[CircularProgressIndicator(), Text("Signing In")],
        )),
      ),
      context: context,
    );
  }

  login(context, url, userData, {secureKey}) async {
    showCircularProgressIndicator(context);
    isProcessing(true);
    setData(userData);
    setUrl(url);
    var res = await loginRequest();
    if (res['hasError']) {
      Navigator.pop(context);
      isProcessing(false);
      onError(context, res['message']);
    } else if (!res['hasInternet']) {
      Navigator.pop(context);
      isProcessing(false);
      onNoInternetConnection(context, res['message']);
    } else {
      Navigator.pop(context);
      isProcessing(false);
      onSuccess(context);
      if (secureKey != null)
        storeToSecureStorage(res['data']['jwt'], secureKey);
    }
  }

  onError(BuildContext context, data) {
    return showDialog(
        context: context,
        child: AlertDialog(
          title: Center(child: Text(data)),
        ));
  }

  onNoInternetConnection(context, data) {
    return showDialog(
        context: context,
        child: AlertDialog(
          title: Center(child: Text(data)),
        ));
  }

  onSuccess(context) {
    showDialog(
        context: context,
        child: AlertDialog(
          title: Center(child: Text('Sucessfully logged in')),
        ));
  }

  void storeToSecureStorage(String data, String key) async {
    await storage.write(key: key, value: data);
    readFromSecureStorage(key);
  }

  void readFromSecureStorage(key) async {
    token = await storage.read(key: key);
  }
}
