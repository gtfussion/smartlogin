import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';

import 'ServiceClass.dart';
import 'locator.dart';

void main() {
  setupLocator();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: PackageExample(),
    );
  }
}

class PackageExample extends StatelessWidget {
  final f1 = new TextEditingController();
  final f2 = new TextEditingController();
  // Api _api = locator<Api>();
  Login myservice = locator<Login>();
  Map userData = new Map();
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Container(
          child: Form(
              child: Column(children: [
            Text('UserName'),
            TextFormField(
              controller: f1,
              autofocus: true,
              onChanged: (value) {
                userData['email'] = value;
              },
            ),
            Text('Password'),
            TextFormField(
              autofocus: true,
              onChanged: (value) {
                userData['password'] = value;
              },
            ),
            RaisedButton(
                color: Colors.green,
                child: Text('Submit'),
                onPressed: () async {
                  // var auth = {auuserdata};
                  await myservice.login(
                    context,
                    'https://school.nivid.app/tokenuser_token',
                    {'auth': userData},
                    secureKey: 'jwt',
                  );
                }),
            Text(myservice.getToken ?? '')
          ])),
        ),
      ),
    );
  }
}
