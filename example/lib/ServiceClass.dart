import 'package:flutter/material.dart';
import 'package:smartLogin/smartLogin.dart';
import 'package:provider/provider.dart';

class Login extends LoginService {
  @override
  onSuccess(context) {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => SecondPage()),
    );
  }
}

class SecondPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Center(child: Text('Well done')),
    );
  }
}
